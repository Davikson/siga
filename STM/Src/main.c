/**
  ******************************************************************************
  * File Name          : main.c
  * Description        : Main program body
  ******************************************************************************
  *
  * COPYRIGHT(c) 2017 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"

/* USER CODE BEGIN Includes */
#include "stdlib.h"
/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;
DMA_HandleTypeDef hdma_adc1;

I2C_HandleTypeDef hi2c1;

TIM_HandleTypeDef htim1;
TIM_HandleTypeDef htim4;

UART_HandleTypeDef huart1;
DMA_HandleTypeDef hdma_usart1_rx;
DMA_HandleTypeDef hdma_usart1_tx;

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
char Received;
uint16_t Czujnik_raw[8];
uint8_t Czujnik[8];
uint16_t pomiar_napiecia, prog;
int16_t uchyb_poprzeni=0, blad, blad_poprzedni=0, przestrzelony;
uint16_t Kp=1, Kd=1;
uint8_t htim4_flag=0;

uint8_t ACC_raw[2];
uint16_t ACC=0;

#define MMA8453Q_ACC_ADDRESS (0x1D << 1) //adres akcelerometru : 0011 001x
// CTRL_REG1_A = [aslp1][aslp0][DR2][DR1][DR0][lnoise][f_read][active]
#define MMA8453Q_ACC_CTRL_REG1_A 0x2a // rejestr 1
#define MMA8453Q_ACC_100HZ 0x18 //maska 00011000
#define MMA8453Q_ACC_Active 0x01 //maska 00000001

#define MMA8453Q_ACC_Z_L_A 0x06 //LSB danych osi Z
#define MMA8453Q_ACC_Z_M_A 0x05 //MSB danych osi Z

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_TIM1_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_ADC1_Init(void);
static void MX_I2C1_Init(void);
//static void SystemClock_Config(void);
static void MX_TIM4_Init(void);

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);
                

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef * htim)
{
	if(htim == &htim4)
	{
		htim4_flag = 1;
	}
}
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) {

	case 22: // Jezeli odebrany zostanie znak 22
		HAL_GPIO_WritePin(GPIOB, diody_tyl_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(AIN1_GPIO_Port, AIN1_Pin,1);
		HAL_GPIO_WritePin(AIN2_GPIO_Port, AIN2_Pin,0);

		HAL_GPIO_WritePin(BIN1_GPIO_Port, BIN1_Pin,0);
		HAL_GPIO_WritePin(BIN2_GPIO_Port, BIN2_Pin,1);

		htim1.Instance->CCR1=351;
		htim1.Instance->CCR4=351;
		break;

	case 24: // Jezeli odebrany zostanie znak 24
		HAL_GPIO_WritePin(GPIOB, diody_tyl_Pin, GPIO_PIN_RESET);
		htim1.Instance->CCR1=350;
		htim1.Instance->CCR4=220;
		break;

	case 25: // Jezeli odebrany zostanie znak 25 STOP
		HAL_GPIO_WritePin(GPIOB, diody_tyl_Pin, GPIO_PIN_SET);
		htim1.Instance->CCR1=0;
		htim1.Instance->CCR4=0;
		break;

	case 26: // Jezeli odebrany zostanie znak 26
		HAL_GPIO_WritePin(GPIOB, diody_tyl_Pin, GPIO_PIN_RESET);
		htim1.Instance->CCR1=220;
		htim1.Instance->CCR4=350;
		break;

	case 28: // Jezeli odebrany zostanie znak 28
		htim1.Instance->CCR1=250;
		htim1.Instance->CCR4=250;
		HAL_GPIO_WritePin(GPIOB, diody_tyl_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(BIN1_GPIO_Port, BIN1_Pin,1);
		HAL_GPIO_WritePin(BIN2_GPIO_Port, BIN2_Pin,0);

		HAL_GPIO_WritePin(AIN1_GPIO_Port, AIN1_Pin,0);
		HAL_GPIO_WritePin(AIN2_GPIO_Port, AIN2_Pin,1);
		break;

	case 21:
		tim=1;
		break;

	case 23:
		tim=0;
		break;

	case 10: //autonomous
		htim4_flag=1;
		kalibracja();
		break;

	case 11: //manual
		htim4_flag=0;
		break;

	case 12: //kd_up
		Kd+=1;
		HAL_GPIO_WritePin(dioda1_GPIO_Port, dioda1_Pin, GPIO_PIN_SET);
		for(int i=0; i<99999; i++); {}
		HAL_GPIO_WritePin(dioda1_GPIO_Port, dioda1_Pin, GPIO_PIN_RESET);
		break;

	case 13:
		Kd-=1;
		HAL_GPIO_WritePin(dioda1_GPIO_Port, dioda1_Pin, GPIO_PIN_SET);
		for(int i=0; i<99999; i++); {}
		HAL_GPIO_WritePin(dioda1_GPIO_Port, dioda1_Pin, GPIO_PIN_RESET);
		break;

	case 14:
		Kp+=1;
		HAL_GPIO_WritePin(dioda2_GPIO_Port, dioda2_Pin, GPIO_PIN_SET);
		for(int i=0; i<99999; i++); {}
		HAL_GPIO_WritePin(dioda2_GPIO_Port, dioda2_Pin, GPIO_PIN_RESET);
		break;

	case 15:
		Kp-=1;
		HAL_GPIO_WritePin(dioda2_GPIO_Port, dioda2_Pin, GPIO_PIN_SET);
		for(int i=0; i<99999; i++); {}
		HAL_GPIO_WritePin(dioda2_GPIO_Port, dioda2_Pin, GPIO_PIN_RESET);
		break;
	default: // Jezeli odebrano nieobslugiwany znak
		break;
	}
	  HAL_UART_Receive_DMA(&huart1, &Received, 2); // Ponowne w��czenie
}									// 						nas�uchiwania

void kalibracja()
{
	prog=Czujnik_raw[3]+(Czujnik_raw[4]-Czujnik_raw[3])/2; //Czujnik[4] czarny; Czujnik[3] bia�y
}

void progowanie()
{
	for(int i=0; i<8; i++)
	{
		if(Czujnik_raw[i]>prog)
			Czujnik[i]=0;
		else
			Czujnik[i]=1;
	}
}

void licz_blad()
{
	int uchyb=0;
	int ilosc=0;
	int waga=0;

	if(przestrzelony)
		waga=5;
	for(int j=0; j<8; j++)
	{
		uchyb+=Czujnik[j]*(j-3)*waga;
		ilosc+=Czujnik[j];
	}
	if(ilosc!=0)
	{
		uchyb/=ilosc;
		uchyb_poprzeni=uchyb;
	}
	else
	{
		if(uchyb_poprzeni<-20)
		{
			uchyb=-40;
			przestrzelony=1;
		}
		else if(uchyb_poprzeni>20)
		{
			uchyb=40;
			przestrzelony=2;
		}
		else uchyb=0;
	}

	if(przestrzelony==1 && uchyb >= 0)
		przestrzelony=0;
	else if(przestrzelony==2 && uchyb <= 0)
		przestrzelony=0;

	blad = uchyb;
}

int PD()
{
	int rozniczka=blad-blad_poprzedni;
	blad_poprzedni=blad;
	return Kp*blad+Kd*rozniczka;
}

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

int main(void)
{

  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* Configure the system clock */
  SystemClock_Config();

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_TIM1_Init();
  MX_USART1_UART_Init();
  MX_ADC1_Init();
  MX_I2C1_Init();
  SystemClock_Config();
  MX_TIM4_Init();

  /* USER CODE BEGIN 2 */
  //konfiguracja akcelerometru
  uint8_t settings = MMA8453Q_ACC_CTRL_REG1_A | MMA8453Q_ACC_Active;
  HAL_I2C_Mem_Write(&hi2c1, MMA8453Q_ACC_ADDRESS, MMA8453Q_ACC_CTRL_REG1_A, 1, &settings, 1, 100);


  //konfiguracja PWM
  HAL_GPIO_WritePin(PWMA_GPIO_Port, PWMA_Pin, GPIO_PIN_SET);
  HAL_TIMEx_PWMN_Start(&htim1,TIM_CHANNEL_1);
  HAL_GPIO_WritePin(AIN1_GPIO_Port, AIN1_Pin, GPIO_PIN_SET);
  HAL_GPIO_WritePin(AIN2_GPIO_Port, AIN2_Pin, GPIO_PIN_RESET);

  HAL_GPIO_WritePin(PWMB_GPIO_Port, PWMB_Pin, GPIO_PIN_SET);
  HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_4);
  HAL_GPIO_WritePin(BIN1_GPIO_Port, BIN1_Pin, GPIO_PIN_RESET);
  HAL_GPIO_WritePin(BIN2_GPIO_Port, BIN2_Pin, GPIO_PIN_SET);

  htim1.Instance->CCR1=0;
  htim1.Instance->CCR4=0;

  //konfiguracja DMA
  HAL_ADC_Start_DMA(&hadc1, Czujnik_raw, 8);
  HAL_UART_Receive_DMA(&huart1, &Received, 1);

  //uruchomienie timera
  HAL_TIM_Base_Start_IT(&htim4);

  kalibracja();
  uint16_t predkosc = 200;

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
	  //HAL_I2C_Mem_Read(&hi2c1, MMA8453Q_ACC_ADDRESS, MMA8453Q_ACC_Z_M_A, 1, ACC_raw, 2, 100);
	  //ACC = (ACC_raw[0] << 8) + ACC_raw[1];

	  if(HAL_GPIO_ReadPin(przycisk2_GPIO_Port, przycisk2_Pin)==GPIO_PIN_RESET)
	  {
		  kalibracja();
		  HAL_GPIO_WritePin(dioda1_GPIO_Port, dioda1_Pin, GPIO_PIN_SET);
		  htim4_flag =1;
	  }
	  else
	  {
		  HAL_GPIO_WritePin(dioda1_GPIO_Port, dioda1_Pin, GPIO_PIN_RESET);
	  }

	  if(tim==1)
	  {
		  timer++;
	  }

	  if(htim4_flag==1)
	  {
		  progowanie();
		  licz_blad();
		  regulacja=PD();
		  lewy=predkosc+regulacja;
		  prawy=predkosc-regulacja;

		  htim1.Instance->CCR1=predkosc+regulacja;
		  htim1.Instance->CCR4=predkosc-regulacja;
	  }
	  if(timer==10000)
	  {
		  size=sprintf(Data,"@%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d$",lewy,prawy,Czujnik_raw[1],Czujnik_raw[2],Czujnik_raw[3],Czujnik_raw[4],Czujnik_raw[5],Czujnik_raw[6],Czujnik_raw[7],Kd,Kp,Czujnik[0]);
		  HAL_UART_Transmit_DMA(&huart1, Data,size);
		  timer=0;
	  }
	  else if(htim4_flag==0)
	  {
		  htim1.Instance->CCR1=0;
		  htim1.Instance->CCR4=0;
		  htim4_flag=2;
	  }
  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */

  }
  /* USER CODE END 3 */

}

/** System Clock Configuration
*/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;

  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI_DIV2;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL16;
  HAL_RCC_OscConfig(&RCC_OscInitStruct);

  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;
  HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2);

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_ADC;
  PeriphClkInit.AdcClockSelection = RCC_ADCPCLK2_DIV6;
  HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit);

  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* ADC1 init function */
void MX_ADC1_Init(void)
{

  ADC_ChannelConfTypeDef sConfig;

    /**Common config 
    */
  hadc1.Instance = ADC1;
  hadc1.Init.ScanConvMode = ADC_SCAN_ENABLE;
  hadc1.Init.ContinuousConvMode = ENABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 8;
  HAL_ADC_Init(&hadc1);

    /**Configure Regular Channel 
    */
  sConfig.Channel = ADC_CHANNEL_0;
  sConfig.Rank = 1;
  sConfig.SamplingTime = ADC_SAMPLETIME_239CYCLES_5;
  HAL_ADC_ConfigChannel(&hadc1, &sConfig);

    /**Configure Regular Channel 
    */
  sConfig.Channel = ADC_CHANNEL_3;
  sConfig.Rank = 2;
  HAL_ADC_ConfigChannel(&hadc1, &sConfig);

    /**Configure Regular Channel 
    */
  sConfig.Channel = ADC_CHANNEL_4;
  sConfig.Rank = 3;
  HAL_ADC_ConfigChannel(&hadc1, &sConfig);

    /**Configure Regular Channel 
    */
  sConfig.Channel = ADC_CHANNEL_5;
  sConfig.Rank = 4;
  HAL_ADC_ConfigChannel(&hadc1, &sConfig);

    /**Configure Regular Channel 
    */
  sConfig.Channel = ADC_CHANNEL_6;
  sConfig.Rank = 5;
  HAL_ADC_ConfigChannel(&hadc1, &sConfig);

    /**Configure Regular Channel 
    */
  sConfig.Channel = ADC_CHANNEL_7;
  sConfig.Rank = 6;
  HAL_ADC_ConfigChannel(&hadc1, &sConfig);

    /**Configure Regular Channel 
    */
  sConfig.Channel = ADC_CHANNEL_8;
  sConfig.Rank = 7;
  HAL_ADC_ConfigChannel(&hadc1, &sConfig);

    /**Configure Regular Channel 
    */
  sConfig.Channel = ADC_CHANNEL_9;
  sConfig.Rank = 8;
  HAL_ADC_ConfigChannel(&hadc1, &sConfig);

}

/* I2C1 init function */
void MX_I2C1_Init(void)
{

  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 400000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLED;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLED;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLED;
  HAL_I2C_Init(&hi2c1);

}

/* TIM1 init function */
void MX_TIM1_Init(void)
{

  TIM_ClockConfigTypeDef sClockSourceConfig;
  TIM_MasterConfigTypeDef sMasterConfig;
  TIM_OC_InitTypeDef sConfigOC;
  TIM_BreakDeadTimeConfigTypeDef sBreakDeadTimeConfig;

  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 63;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 999;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  HAL_TIM_Base_Init(&htim1);

  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  HAL_TIM_ConfigClockSource(&htim1, &sClockSourceConfig);

  HAL_TIM_PWM_Init(&htim1);

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig);

  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCNPolarity = TIM_OCNPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
  sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
  HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_1);

  HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_4);

  sBreakDeadTimeConfig.OffStateRunMode = TIM_OSSR_DISABLE;
  sBreakDeadTimeConfig.OffStateIDLEMode = TIM_OSSI_DISABLE;
  sBreakDeadTimeConfig.LockLevel = TIM_LOCKLEVEL_OFF;
  sBreakDeadTimeConfig.DeadTime = 0;
  sBreakDeadTimeConfig.BreakState = TIM_BREAK_DISABLE;
  sBreakDeadTimeConfig.BreakPolarity = TIM_BREAKPOLARITY_HIGH;
  sBreakDeadTimeConfig.AutomaticOutput = TIM_AUTOMATICOUTPUT_DISABLE;
  HAL_TIMEx_ConfigBreakDeadTime(&htim1, &sBreakDeadTimeConfig);

  HAL_TIM_MspPostInit(&htim1);

}

/* TIM4 init function */
void MX_TIM4_Init(void)
{

  TIM_ClockConfigTypeDef sClockSourceConfig;
  TIM_MasterConfigTypeDef sMasterConfig;

  htim4.Instance = TIM4;
  htim4.Init.Prescaler = 639;
  htim4.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim4.Init.Period = 999;
  htim4.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  HAL_TIM_Base_Init(&htim4);

  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  HAL_TIM_ConfigClockSource(&htim4, &sClockSourceConfig);

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  HAL_TIMEx_MasterConfigSynchronization(&htim4, &sMasterConfig);

}

/* USART1 init function */
void MX_USART1_UART_Init(void)
{

  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  HAL_UART_Init(&huart1);

}

/** 
  * Enable DMA controller clock
  */
void MX_DMA_Init(void) 
{
  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  HAL_NVIC_SetPriority(DMA1_Channel1_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel1_IRQn);
  HAL_NVIC_SetPriority(DMA1_Channel4_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel4_IRQn);
  HAL_NVIC_SetPriority(DMA1_Channel5_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel5_IRQn);

}

/** Configure pins as 
        * Analog 
        * Input 
        * Output
        * EVENT_OUT
        * EXTI
*/
void MX_GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __GPIOC_CLK_ENABLE();
  __GPIOD_CLK_ENABLE();
  __GPIOA_CLK_ENABLE();
  __GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, dioda1_Pin|dioda2_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOD, dioda_przod_Pin|dioda_przod2_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, AIN2_Pin|AIN1_Pin|diody_tyl_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, BIN1_Pin|BIN2_Pin|INT_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : dioda1_Pin dioda2_Pin */
  GPIO_InitStruct.Pin = dioda1_Pin|dioda2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : dioda_przod_Pin dioda_przod2_Pin */
  GPIO_InitStruct.Pin = dioda_przod_Pin|dioda_przod2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pins : przycisk2_Pin przycisk1_Pin */
  GPIO_InitStruct.Pin = przycisk2_Pin|przycisk1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : AIN2_Pin AIN1_Pin diody_tyl_Pin */
  GPIO_InitStruct.Pin = AIN2_Pin|AIN1_Pin|diody_tyl_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : BIN1_Pin BIN2_Pin INT_Pin */
  GPIO_InitStruct.Pin = BIN1_Pin|BIN2_Pin|INT_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure peripheral I/O remapping */
  __HAL_AFIO_REMAP_PD01_ENABLE();

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
