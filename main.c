/**
  ******************************************************************************
  * File Name          : main.c
  * Description        : Main program body
  ******************************************************************************
  *
  * COPYRIGHT(c) 2017 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"
#include "adc.h"
#include "dma.h"
#include "i2c.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
char Received;
uint16_t Czujnik_raw[8];
uint16_t Czujnik[8];
uint16_t pomiar_napiecia, prog=0;
uint16_t uchyb_poprzedni=0, blad, blad_poprzedni=0, przestrzelony;
uint16_t Kp=1, Kd=0;
uint16_t htim4_flag=0;
uint8_t Data[40];
uint16_t size=0;
uint16_t prawy=0, lewy=0;
uint16_t predkosc=200;
uint16_t uchyb =0;
uint16_t ilosc=0;
uint16_t waga=10;
int16_t regulacja=0;
uint16_t rozniczka=0;

#define MMA8453Q_ACC_ADDRESS (0x1D << 1) //adres akcelerometru : 0011 001x
// CTRL_REG1_A = [aslp1][aslp0][DR2][DR1][DR0][lnoise][f_read][active]
#define MMA8453Q_ACC_CTRL_REG1_A 0x2a // rejestr 1
#define MMA8453Q_ACC_100HZ 0x18 //maska bitowa 00011000

#define MMA8453Q_ACC_Z_L_A 0x06 //nizszy bajt danych osi Z
#define MMA8453Q_ACC_Z_M_A 0x05 //wyzszy bajt danych osi Z

//uint8_t Data = 0; // Zmienna do bezposredniego odczytu z akcelerometru
int16_t Zaxis = 0; // Zawiera przeksztalcona forme odczytanych danych


/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) {

	switch (atoi(&Received)) {

	case 2: // Jezeli odebrany zostanie znak 2
		HAL_GPIO_WritePin(GPIOB, diody_tyl_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(AIN1_GPIO_Port, AIN1_Pin,1);
		HAL_GPIO_WritePin(AIN2_GPIO_Port, AIN2_Pin,0);

		HAL_GPIO_WritePin(BIN1_GPIO_Port, BIN1_Pin,0);
		HAL_GPIO_WritePin(BIN2_GPIO_Port, BIN2_Pin,1);

		htim1.Instance->CCR1=351;
		htim1.Instance->CCR4=351;
		prawy=htim1.Instance->CCR1;
		lewy=htim1.Instance->CCR4;
		size=sprintf(Data,"@%d,%d,2000,2000,4000,2000,2000,2000,2000$",lewy,prawy);
		//size=sprintf(Data, "@lewy,prawy$", lewy, prawy);
		HAL_UART_Transmit_DMA(&huart1, Data,size);
		break;

	case 4: // Jezeli odebrany zostanie znak 4
		HAL_GPIO_WritePin(GPIOB, diody_tyl_Pin, GPIO_PIN_RESET);
		htim1.Instance->CCR1=350;
		htim1.Instance->CCR4=220;
		//printf("@slewy=%d$",htim1.Instance->CCR1);
		//printf("@sprawy=%d$",htim1.Instance->CCR4);
		break;

	case 5: // Jezeli odebrany zostanie znak 5 STOP
		HAL_GPIO_WritePin(GPIOB, diody_tyl_Pin, GPIO_PIN_SET);
		htim1.Instance->CCR1=0;
		htim1.Instance->CCR4=0;
		//printf("@slewy=%d$",htim1.Instance->CCR1);
		//printf("@sprawy=%d$",htim1.Instance->CCR4);
		break;

	case 6: // Jezeli odebrany zostanie znak 6
		HAL_GPIO_WritePin(GPIOB, diody_tyl_Pin, GPIO_PIN_RESET);
		htim1.Instance->CCR1=220;
		htim1.Instance->CCR4=350;
		//printf("@slewy=%d$",htim1.Instance->CCR1);
		//printf("@sprawy=%d$",htim1.Instance->CCR4);
		break;

	case 8: // Jezeli odebrany zostanie znak 8
		htim1.Instance->CCR1=250;
		htim1.Instance->CCR4=250;
		HAL_GPIO_WritePin(GPIOB, diody_tyl_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(BIN1_GPIO_Port, BIN1_Pin,1);
		HAL_GPIO_WritePin(BIN2_GPIO_Port, BIN2_Pin,0);

		HAL_GPIO_WritePin(AIN1_GPIO_Port, AIN1_Pin,0);
		HAL_GPIO_WritePin(AIN2_GPIO_Port, AIN2_Pin,1);
		//printf("@slewy=%u$",htim1.Instance->CCR1);
		//printf("@sprawy=%u$",htim1.Instance->CCR4);
		break;
	default: // Jezeli odebrano nieobslugiwany znak
		break;
	}
	  //HAL_UART_Transmit_DMA(&huart1, Data, );
	  HAL_UART_Receive_DMA(&huart1, &Received, 1); // Ponowne w��czenie
}									// 						nas�uchiwania
/*
int _write(int file, char *ptr, int len)
{
	HAL_UART_Transmit_DMA(&huart1, ptr, len);

	return len;
}*/

void kalibracja()
{
	prog=Czujnik[3]+((Czujnik[4]-Czujnik[3])/2); //cz 4- czarny, cz 3- bia�y
}

void progowanie()
{
	for (int i=1; i<8; i++)
	{
		if(Czujnik[i]>prog)
			Czujnik_raw[i]=1;
		else
			Czujnik_raw[i]=0;
	}
}



void licz_blad()
{
	uchyb =0;
	ilosc=0;
	waga=20;

	if(przestrzelony) waga=5;

	for(int j=1; j<8; j++)
	{
		uchyb += Czujnik_raw[j]*(j-4)*waga;
		ilosc += Czujnik_raw[j];
	}

	if(ilosc != 0)
	{
		uchyb /= ilosc;
		uchyb_poprzedni= uchyb;
	}
	else
	{
		if(uchyb_poprzedni<-20)
		{
			uchyb=-40;
			przestrzelony=1;
		}
		else if(uchyb_poprzedni>20)
		{
			uchyb=40;
			przestrzelony=2;
		}
		else uchyb=0;
	}

	if(przestrzelony==1 && uchyb >= 0) przestrzelony=0;
	else if(przestrzelony==2 && uchyb <= 0) przestrzelony=0;

	blad=uchyb;
}

int PD()
{
	rozniczka=blad-blad_poprzedni;
	blad_poprzedni=blad;
	return Kp*blad+Kd*rozniczka;
}


/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

int main(void)
{

  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* Configure the system clock */
  SystemClock_Config();

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_TIM1_Init();
  MX_USART1_UART_Init();
  MX_ADC1_Init();
  MX_I2C1_Init();
  SystemClock_Config();

  /* USER CODE BEGIN 2 */

  //uint8_t Settings = MMA8453Q_ACC_100HZ;

// Wpisanie konfiguracji do rejestru akcelerometru
//	HAL_I2C_Mem_Write(&hi2c1, MMA8453Q_ACC_ADDRESS, MMA8453Q_ACC_CTRL_REG1_A, 1, MMA8453Q_ACC_100HZ, 1, 100);

 HAL_GPIO_WritePin(PWMA_GPIO_Port, PWMA_Pin,GPIO_PIN_SET);
 HAL_TIMEx_PWMN_Start(&htim1,TIM_CHANNEL_1);//uruchomienie timera
 HAL_GPIO_WritePin(AIN1_GPIO_Port, AIN1_Pin,1);// ___\  Ustawia w�a�ciwy
 HAL_GPIO_WritePin(AIN2_GPIO_Port, AIN2_Pin,0);//    /    kierunek obrot�w ko�a

 HAL_GPIO_WritePin(PWMB_GPIO_Port, PWMB_Pin,GPIO_PIN_SET);
 HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_4); //uruchomienie timera
 HAL_GPIO_WritePin(BIN1_GPIO_Port, BIN1_Pin,0);// ___\  Ustawia w�a�ciwy
 HAL_GPIO_WritePin(BIN2_GPIO_Port, BIN2_Pin,1);//    /    kierunek obrot�w ko�a

 HAL_UART_Receive_DMA(&huart1, &Received, 1); //w��cza ci�g�e odczytywanie z uarta
 HAL_ADC_Start_DMA(&hadc1, &Czujnik, 8); //uruchamnia odczyt z czujnik�w
//	HAL_UART_Transmit_DMA(&huart1, Data, 4);

 Czujnik[0]=pomiar_napiecia;

  htim1.Instance->CCR1=0;
  htim1.Instance->CCR4=0;

/*
  Kp=1;
  Kd=4;
  int pop_Kp=0;
  int pop_Kd=0;
  uchyb=0;
*/


  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
	  if(HAL_GPIO_ReadPin(przycisk2_GPIO_Port, przycisk2_Pin)==GPIO_PIN_RESET)
	  {
		  kalibracja();
		  HAL_GPIO_WritePin(dioda1_GPIO_Port, dioda1_Pin, GPIO_PIN_SET);
		  htim4_flag =1;
	  }
	  else
	  {
		  HAL_GPIO_WritePin(dioda1_GPIO_Port, dioda1_Pin, GPIO_PIN_RESET);

	  }

	  if(HAL_GPIO_ReadPin(przycisk1_GPIO_Port, przycisk1_Pin)==GPIO_PIN_RESET)
	  {
		  htim4_flag =1;
		  HAL_GPIO_WritePin(dioda2_GPIO_Port, dioda2_Pin, GPIO_PIN_SET);
	  }


	  if(htim4_flag==1)
	  {
		  progowanie();
		  licz_blad();
		  regulacja=PD();
lewy=predkosc+regulacja;
prawy=predkosc-regulacja;

		  htim1.Instance->CCR1=predkosc+regulacja;
		  htim1.Instance->CCR4=predkosc-regulacja;
	  }



	  /*
	  if(pop_Kd!=Kd || pop_Kp!=Kp)
	  {
		  size=sprintf(Data, "Kd=%d\nKp=%d\n", Kd, Kp);
		  HAL_UART_Transmit_DMA(&huart1, Data, size);
		  pop_Kd=Kd;
		  pop_Kp=Kp;
	  }*/
	 // size=sprintf(Data, "@12,400,2000,2000,4000,2000,2000,2000,2000$",size);
	 // HAL_UART_Transmit_DMA(&huart1, Data, size);

	  //HAL_I2C_Mem_Read(&hi2c1, MMA8453Q_ACC_ADDRESS, (MMA8453Q_ACC_Z_M_A), 1, &Data, 1, 100);
	  //Zaxis = Data << 8;

	  /*
	  adc();
	  blad=licz_blad();
	  int regulacja=PD();

	  htim1.Instance->CCR1=predkosc+regulacja;
	  htim1.Instance->CCR4=predkosc-regulacja;
*/
	  /*

if(Czujnik[7]>wart_prog)
{
	 htim1.Instance->CCR1=300;
	 htim1.Instance->CCR4=0;
}
if(Czujnik[6]>wart_prog)
{
	 htim1.Instance->CCR1=300;
	 htim1.Instance->CCR4=100;

}
if(Czujnik[5]>wart_prog)
{
	 htim1.Instance->CCR1=300;
	 htim1.Instance->CCR4=250;
}
if(Czujnik[4]>wart_prog)
{
	 htim1.Instance->CCR1=300;
	 htim1.Instance->CCR4=300;
	 HAL_GPIO_WritePin(GPIOB, diody_tyl_Pin, GPIO_PIN_SET);
}
if(Czujnik[3]>wart_prog)
{
	 htim1.Instance->CCR1=250;
	 htim1.Instance->CCR4=300;
}
if(Czujnik[2]>wart_prog)
{
	 htim1.Instance->CCR1=100;
	 htim1.Instance->CCR4=300;
}
if(Czujnik[1]>wart_prog)
{
	 htim1.Instance->CCR1=0;
	 htim1.Instance->CCR4=300;
}


*/


  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */

  }
  /* USER CODE END 3 */

}

/** System Clock Configuration
*/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;

  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI_DIV2;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL16;
  HAL_RCC_OscConfig(&RCC_OscInitStruct);

  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;
  HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2);

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_ADC;
  PeriphClkInit.AdcClockSelection = RCC_ADCPCLK2_DIV6;
  HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit);

  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* USER CODE BEGIN 4 */
/*HAL_GPIO_WritePin(GPIOA, GPIO_PIN_11, GPIO_PIN_SET);*/
/* USER CODE END 4 */

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
