/**
  ******************************************************************************
  * File Name          : mxconstants.h
  * Description        : This file contains the common defines of the application
  ******************************************************************************
  *
  * COPYRIGHT(c) 2017 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/

#define dioda1_Pin GPIO_PIN_14
#define dioda1_GPIO_Port GPIOC
#define dioda2_Pin GPIO_PIN_15
#define dioda2_GPIO_Port GPIOC
#define dioda_przod_Pin GPIO_PIN_0
#define dioda_przod_GPIO_Port GPIOD
#define dioda_przod2_Pin GPIO_PIN_1
#define dioda_przod2_GPIO_Port GPIOD
#define pomiar_napiecia_Pin GPIO_PIN_0
#define pomiar_napiecia_GPIO_Port GPIOA
#define przycisk2_Pin GPIO_PIN_1
#define przycisk2_GPIO_Port GPIOA
#define przycisk1_Pin GPIO_PIN_2
#define przycisk1_GPIO_Port GPIOA
#define czujnik1_Pin GPIO_PIN_3
#define czujnik1_GPIO_Port GPIOA
#define czujnik2_Pin GPIO_PIN_4
#define czujnik2_GPIO_Port GPIOA
#define czujnik3_Pin GPIO_PIN_5
#define czujnik3_GPIO_Port GPIOA
#define czujnik4_Pin GPIO_PIN_6
#define czujnik4_GPIO_Port GPIOA
#define czujnik5_Pin GPIO_PIN_7
#define czujnik5_GPIO_Port GPIOA
#define czujnik6_Pin GPIO_PIN_0
#define czujnik6_GPIO_Port GPIOB
#define czujnik7_Pin GPIO_PIN_1
#define czujnik7_GPIO_Port GPIOB
#define PWMA_Pin GPIO_PIN_13
#define PWMA_GPIO_Port GPIOB
#define AIN2_Pin GPIO_PIN_14
#define AIN2_GPIO_Port GPIOB
#define AIN1_Pin GPIO_PIN_15
#define AIN1_GPIO_Port GPIOB
#define BIN1_Pin GPIO_PIN_9
#define BIN1_GPIO_Port GPIOA
#define BIN2_Pin GPIO_PIN_10
#define BIN2_GPIO_Port GPIOA
#define PWMB_Pin GPIO_PIN_11
#define PWMB_GPIO_Port GPIOA
#define INT_Pin GPIO_PIN_15
#define INT_GPIO_Port GPIOA
#define diody_tyl_Pin GPIO_PIN_3
#define diody_tyl_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
